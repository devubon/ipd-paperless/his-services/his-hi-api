import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';

import { ServiceModels } from '../../models/services/services';
import patientSchema from '../../schema/service/service_patient';
import anSchema from '../../schema/service/service_an';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const serviceModels = new ServiceModels();

  fastify.get('/patient', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: patientSchema,
        // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const _query: any = request.query;
      const { limit, offset, an } = _query;
      const _limit = limit || 20;
      const _offset = offset || 0;
      const _an = an || null;
      const data: any = await serviceModels.patient(db,_an,_limit,_offset);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/review', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.review(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/treatement', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.treatement(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/admit', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.admit(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/lab-result', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    // schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an,code } = _query;
      const _an = an || null;
      const _items_code = code || null;
      const data: any = await serviceModels.lab_result(db,_an,_items_code);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/lab-finding', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    // schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.lab_finding(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });


  fastify.get('/xray-result', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    // schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an,items_code } = _query;
      const _an = an || null;
      const _items_code = items_code || null;
      const data: any = await serviceModels.xray_result(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/ekg', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.ekg_file(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/patient-allergy', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.patient_allergy(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });


  done();
} 
